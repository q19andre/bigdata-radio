from __future__ import print_function
from re import L
import sys
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import from_json, col, udf, struct
import cartopy.crs as ccrs
import cartopy.feature as cf
from cartopy.feature import ShapelyFeature
import cartopy.io.shapereader as shpreader
from shapely.geometry import Point

spark = SparkSession.builder.appName("Spark Structured Streaming from Kafka").getOrCreate()
sdfPlanes = spark.readStream.format("kafka") \
    .option("kafka.bootstrap.servers", "pi-node07:9092, pi-node07:9093") \
    .option("subscribe", "air-traffic") \
    .option("startingOffsets", "latest") \
    .load().selectExpr("CAST(value AS STRING)")

planesSchema = StructType([
    StructField("on_ground", BooleanType()), 
    StructField("icao24", IntegerType()),
    StructField("sensors", LongType()), 
    StructField("vertical_rate", FloatType()),
    StructField("origin_country", StringType()), 
    StructField("squawk", LongType()), 
    StructField("geo_altitude", FloatType()),
    StructField("baro_altitude", FloatType()), 
    StructField("velocity", FloatType()),
    StructField("latitude", FloatType()), 
    StructField("spi", BooleanType()),
    StructField("position_source", IntegerType()), 
    StructField("last_contact", LongType()),
    StructField("time_position", LongType()), 
    StructField("heading", FloatType()),
    StructField("time", LongType()), 
    StructField("longitude", FloatType()),
    StructField("callsign", StringType()), 
    ])

'''{"on_ground": false, "icao24": "407182", "sensors": null, "vertical_rate": 0, "origin_country": "United Kingdom", 
"squawk": "7755", "geo_altitude": 8214.36, "baro_altitude": 8229.6, "velocity": 176.26, "latitude": 54.1107, "spi": false, 
"position_source": 0, "last_contact": 1646922076, "time_position": 1646922076, "heading": 158.6, "time": 1646922077, 
"longitude": -2.8725, "callsign": "EXS1LY  "}
'''

def parse_data_from_kafka_message(sdf, schema):
    assert sdf.isStreaming == True, "DataFrame doesn't receive streaming data"
    sdf2 = sdf.withColumn("value", from_json("value", schema))
    return sdf2.select(col("value.*"))
    
df = parse_data_from_kafka_message(sdfPlanes, planesSchema)

'''df.write.csv("hdfs://pi-node30:9000/user/pi/planesRecords")'''

###################### treatment #########################

proj = ccrs.PlateCarree()

# Read shape file
shpfilename = shpreader.natural_earth(resolution='10m',
                                      category='cultural',
                                      name='admin_0_countries')
reader = shpreader.Reader(shpfilename)

countries = []
for country in reader.records():
    countries.append( country.attributes["NAME_LONG"], country.geometry )
    
def to_countries() :
    def find_country(long, lat):
        if long is None or lat is None:
            return 'NoCountry'
        point = Point(lat, long)
        for country, geom in countries:
            if geom.contains(point):
                return country
        return 'NoCountry'
    return udf(lambda x: find_country(x[0], x[1]), FloatType())

'''query = df.select([col("value.time_position"), col("value.longitude"), col("value.latitude"), col("value.icao24")]) \
    .map(planes_to_countries) \
    .reduceByKeyAndWindow(lambda x,y: int(x)+int(y), lambda x,y: int(x)-int(y), 60, 15) \
    .writeStream \
    .format("org.apache.spark.sql.cassandra") \
    .outputMode("append") \
    .option("checkpointLocation", "hdfs://pi-node30:9000/user/pi/checkp") \
    .options(table="planes_area", keyspace="planes")\
    .option("truncate", False) \
    .start().awaitTermination()'''

query = df.select(["time_position", "longitude", "latitude", "icao24"]) \
    .withColumn("country", to_countries()(struct("longitude", "latitude"))) \
    .select(["time_position", "country", "icao24"]) \
    .writeStream \
    .format("console") \
    .outputMode("append") \
    .option("checkpointLocation", "hdfs://pi-node30:9000/user/pi/checkp") \
    .option("truncate", False) \
    .start().awaitTermination()

'''
.map(lambda row: ((row[1], row[2]), (row[0], 1)))
.reduceByKeyAndWindow(addCount, substractCount, 3600, 30)
.map(lambda row: (row[0][0], row[1][1]))
.reduceByKey(lambda x,y: x+y, lambda x,y: x-y)
=> (country, count)

def addCount(x,y):
    x_time, x_count = x
    y_time, y_count = y
    if abs(x_time - y_time) > 3600:
        return y_time, x_count+y_count 
    else:
        return y_time, x_count

def substractCount(x,y):
    x_time, x_count = x
    y_time, y_count = y
    if abs(x_time - y_time) > 3600:
        return y_time, x_count-y_count
    else:
        return y_time, x_count
'''