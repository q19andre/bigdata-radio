# create topic node7
ssh pi@10.29.227.217 
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 2 --partitions 10 --topic air-traffic

# delete topics node7
delete.topic.enable=true
kafka-topics.sh --zookeeper localhost:2181 --delete --topic <topic_name>

# reformat node7
rm -rf /tmp/zookeeper/
ls /opt/kafka-data/
sudo rm -rf /opt/kafka-data/
sudo mkdir /opt/kafka-data/
sudo chown -R pi:pi /opt/kafka-data

# listen to a node
nc pi-node07 9092

# launch script (producer) in node8
ssh pi@10.29.227.218 
python3 bigdata-radio/planes_producer.py
# consumer in node8
python3 bigdata-radio/planes_consumer.py
