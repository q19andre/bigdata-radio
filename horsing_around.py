from geopy.geocoders import Nominatim

def find_country(long, lat):
    geolocator = Nominatim(user_agent="quentin.andre@imt-atlantique.net")
    coord = f'{lat}, {long}'
    location = geolocator.reverse(coord, exactly_one=True)
    if location:
        return location.raw['address'].get('country', 'NoCountry')
    else:
        return 'NoCountry'

r = find_country(-22, 45)
print(r)
print(type(r))