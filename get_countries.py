import cartopy.crs as ccrs
import cartopy.feature as cf
from cartopy.feature import ShapelyFeature
import cartopy.io.shapereader as shpreader
from shapely.geometry import Point

proj = ccrs.PlateCarree()

# Read shape file
shpfilename = shpreader.natural_earth(resolution='10m',
                                      category='cultural',
                                      name='admin_0_countries')
reader = shpreader.Reader(shpfilename)

# Filter for a specific country
countries = [country for country in reader.records()]

# Display Kenya's shape
#shape_feature = ShapelyFeature([country.geometry for country in countries], ccrs.PlateCarree(), facecolor="lime", edgecolor='black', lw=1)

boundaries = [country.geometry for country in countries]
print(boundaries[0])
print(boundaries[0].contains(Point(0,0)))