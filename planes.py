from time import sleep, time
import json

from opensky_api import OpenSkyApi
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers="pi-node07:9092")

api = OpenSkyApi()
while 1:
    states = api.get_states()
    for state in states.states:
        plane = {'time': states.time}
        for key in state.keys:
            plane[key] = getattr(state, key)
        producer.send("air-traffic", json.dumps(plane).encode())
    print("{} Produced {} air traffic records".format(time(), len(states.states)))
    sleep(10.01)


if __name__ == '__main__':
    pass
