# node29 is master
ssh pi@10.29.227.239

# launch
start-dfs.sh 
$SPARK_HOME/sbin/start-all.sh
# launch spark_streaming in node 29 
spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0 SparkStreamingRadio/spark_streaming_radio.py

# stop
$SPARK_HOME/sbin/stop-all.sh
stop-dfs.sh