from time import sleep, time
import json

from opensky_api import OpenSkyApi
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers="pi-node07:9092,pi-node07:9093")

api = OpenSkyApi()
while 1:
    states = api.get_states()
    for state in states.states:
        plane = {'time': states.time}
        for key in state.keys:
            plane[key] = getattr(state, key)
        producer.send("air-traffic", json.dumps(plane).encode())
    print("{} Produced {} air traffic records".format(time(), len(states.states)))
    sleep(12)


if __name__ == '__main__':
    pass
